/**
 * \file vector.h
 * \brief Contains routines handling dynamic array generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __VECTOR__
	#define __VECTOR__
	#include <stdio.h>
	#include <stdlib.h>
	#include <stdbool.h>
	#include <errno.h>
	#include <string.h>

	#define DEFINE_VECTOR_TYPE(TYPE, NAME) \
		typedef struct { \
			TYPE * value; \
			size_t cardinal; \
			size_t capacity; \
		} Vector##NAME;

	#define DEFINE_VECTOR_WRAPPER_MALLOC(TYPE, NAME) \
		static Vector##NAME * Vector##NAME##_malloc(size_t capacity) \
		{ \
			return (Vector##NAME *)Vector_malloc(capacity, sizeof(TYPE)); \
		}

	#define DEFINE_VECTOR_WRAPPER_RSHIFT(TYPE, NAME) \
		static void Vector##NAME##_rshift(void * address, size_t shiftSize) \
		{ \
			Vector_rshift(address, shiftSize, sizeof(TYPE)); \
		}

	#define DEFINE_VECTOR_WRAPPER_LSHIFT(TYPE, NAME) \
		static void Vector##NAME##_lshift(TYPE * address, size_t shiftSize) \
		{ \
			Vector_lshift(address, shiftSize, sizeof(TYPE)); \
		}

	#define DEFINE_VECTOR_WRAPPER_CMP(TYPE, NAME) \
		static bool Vector##NAME##_cmp(Vector##NAME * vector0, Vector##NAME * vector1) \
		{ \
			return Vector_cmp((Vector *)vector0, sizeof(TYPE), (Vector *)vector1, sizeof(TYPE)); \
		}

	typedef struct {
		void * value;
		size_t cardinal;
		size_t capacity;
	} Vector;

	typedef unsigned char Byte;

	/**
	 * \fn Vector * Vector_malloc(const size_t capacity, const size_t atomicSize)
	 * \brief Allocates vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param capacity Number of elements.
	 * \param atomicSize size of one element (in byte).
	 * \return Vector address.
	 */
	Vector * Vector_malloc(const size_t capacity, const size_t atomicSize);

	/**
	 * \fn void * Vector_free(void * vectorAddress)
	 * \brief Frees previously allocated vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2019
	 * \param vectorAddress Address of vector to be freed.
	 */
	void * Vector_free(void * vectorAddress);

	/**
	 * \fn void Vector_rshift(void * const address, const size_t shiftSize, const size_t offset)
	 * \brief Shifts all vector value(s) from one element size to the right.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param address A vector->value address.
	 * \param shiftSize Cardinal of a given vector.
	 * \param offset Size of one element (in byte).
	 */
	void Vector_rshift(void * const address, const size_t shiftSize, const size_t offset);

	/**
	 * \fn void Vector_lshift(void * const address, const size_t shiftSize, const size_t offset)
	 * \brief Shifts all vector value(s) from one element size to the left.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param address A vector->value address.
	 * \param shiftSize Cardinal of a given vector.
	 * \param offset Size of one element (in byte).
	 */
	void Vector_lshift(void * const address, const size_t shiftSize, const size_t offset);
	
	/**
	 * \fn bool Vector_cmp(const Vector * const vector0, const size_t atomicSize0, const Vector * const vector1, const size_t atomicSize1);
	 * \brief Compares two given vectors.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 january 2020
	 * \param vector0 vector object.
	 * \param atomicSize0 size of one element of vector0 (in byte).
	 * \param vector1 vector object.
	 * \param atomicSize0 size of one element of vector1 (in byte).
	 * \return true if the two given vectors are equal.
	 */
	bool Vector_cmp(const Vector * const vector0, const size_t atomicSize0, const Vector * const vector1, const size_t atomicSize1);
#endif
