compiler = gcc
objects = action.o  main.o  option.o  secure.o  table.o  transcode.o  vector.o

compress : $(objects)
	$(compiler) -o compress $(objects) -lm

main.o : option.h
option.o : option.h action.h
action.o : action.h table.h secure.h transcode.h vector.h
table.o : table.h secure.h vector.h
secure.o : secure.h
transcode.o : transcode.h
vector.o : vector.h

.PHONY : clean
clean :
	rm compress $(objects)
