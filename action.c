/**
 * \file action.c
 * \brief Routines interacting with files and table.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "action.h"

DEFINE_VECTOR_WRAPPER_MALLOC(unsigned char, UC)
DEFINE_VECTOR_WRAPPER_CMP(unsigned char, UC)

void action_recycle_table(Table * const table)
{
	if (fclose(table->plainFile) == EOF) {
		fprintf(stderr,"error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	table->plainFile = NULL;
	if (fclose(table->compressedFile) == EOF) {
		fprintf(stderr,"error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	table->compressedFile = NULL;
	table_index_free(table);
	table_code_free(table);
}

void action_set_targets(Table * const table, const TableParam param)
{
	const char * plainMode;
	const char * compressedMode;
	plainMode = "rb";
	compressedMode = "wb";
	if (param.action == DECOMPRESS) {
		plainMode = "wb";
		compressedMode = "rb";
	}
	if ((table->plainFile = fopen(param.plainFilePath, plainMode)) == NULL) {
		fprintf(stderr,"error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	if ((table->compressedFile = fopen(param.compressedFilePath, compressedMode)) == NULL) {
		fprintf(stderr,"error : %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

void action_write_freq(Table * const table)
{
	size_t i;
	unsigned short count;
	count = table->index->cardinal;
	secure_fwrite(&count, sizeof(unsigned short), 1, table->compressedFile);
	for (i = 0; i < table->index->cardinal; i++) {
		secure_fputc((int)table->index->value[i], table->compressedFile);
		secure_fwrite(table->freq + table->index->value[i], sizeof(long long), 1, table->compressedFile);
	}
}

void action_readfreq_from_plainfile(Table * const table)
{
	int currChar;
	table->count = 0;
	while ((currChar = secure_fgetc(table->plainFile)) != EOF) {
		if (table->freq[currChar] == ULLONG_MAX) {
			fprintf(stderr, "error, %c occurrences > max %llu occurrences per file", (unsigned char)currChar, ULLONG_MAX);
			exit(EXIT_FAILURE);
		}
		table->freq[currChar]++;
		if (table->freq[currChar] == 1) {
			table->count++;
		}
	}
	rewind(table->plainFile);
}

void action_readfreq_from_compressedfile(Table * const table)
{
	int currChar;
	size_t i;
	unsigned long long currFreq;
	secure_fread(&(table->count), sizeof(unsigned short), 1, table->compressedFile);
	for (i = 0; i < table->count; i++) {
		currChar = secure_fgetc(table->compressedFile);
		secure_fread(&currFreq, sizeof(long long), 1, table->compressedFile);
		table->freq[currChar] = currFreq;
	}
}

void action_prepare_table(Table * const table, const TableParam param)
{
	action_set_targets(table, param);
	switch (param.action) {
		case DECOMPRESS:
			action_readfreq_from_compressedfile(table);
			break;
		default:
			action_readfreq_from_plainfile(table);
			break;
	}
	table_set_index(table);
	table_set_code(table);
}

void action_bin_compress(Table * const table)
{
	int currPlainChar;
	unsigned char strByte[CHAR_BIT];
	long lastBytePos;
	size_t i, j;
	j = 0;
	action_write_freq(table);
	secure_fputc(0,table->compressedFile);
	lastBytePos = secure_ftell(table->compressedFile);
	secure_fputc(0, table->compressedFile);
	secure_fputc(0, table->compressedFile);
	while ((currPlainChar = secure_fgetc(table->plainFile)) != EOF) {
		for (i = 0; i < table->code[currPlainChar]->cardinal; i++, j++) {
			strByte[j % CHAR_BIT] = table->code[currPlainChar]->value[i];
			if ((j % CHAR_BIT) == CHAR_BIT-1) {
				secure_fputc((int)transcode_str_to_byte(strByte), table->compressedFile);
			}
		}
	}
	secure_fseek(table->compressedFile, lastBytePos, SEEK_SET);
	secure_fputc((int)transcode_str_to_byte(strByte), table->compressedFile);
	secure_fputc((int)(j % CHAR_BIT), table->compressedFile);
	secure_fseek(table->compressedFile, 0L, SEEK_END);
}

bool action_write_matched_plainbyte(Table * const table, VectorUC * const code)
{
	size_t i, j;
	char k;
	bool write;
	write = false;
	if (code->cardinal >= table->code[table->index->value[table->count-1]]->cardinal) {
		j = 0;
		k = -1;
		if (1.0*code->cardinal <= (0.5)*table->code[table->index->value[0]]->cardinal) {
			j = table->count-1;
			k = 1;
		}
		i = 0;
		while ((i < table->count) && (! VectorUC_cmp(code,table->code[table->index->value[j - i * k]]))) {
			i++;
		}
		write = i < table->count;
		if (write) {
			secure_fputc(table->index->value[j - i * k], table->plainFile);
		}
	}
	return write;
}

void action_bin_decompress_byte(Table * const table, unsigned char compressedByte, VectorUC * const codeBuffer, const unsigned char dataBitSize)
{
	unsigned char * strByte;
	size_t i;
	strByte = transcode_byte_to_str(compressedByte);
	if (dataBitSize > CHAR_BIT) {
		fprintf(stderr,"error : last byte length is invalid.\n");
		exit(EXIT_FAILURE);
	}
	for (i = 0; i < dataBitSize; i++) {
		codeBuffer->value[codeBuffer->cardinal] = strByte[i];
		codeBuffer->cardinal++;
		if (action_write_matched_plainbyte(table, codeBuffer)) {
			codeBuffer->cardinal = 0;
		} else if (codeBuffer->cardinal > codeBuffer->capacity) {
			fprintf(stderr, "error : compressed file contains invalid code.\n");
			exit(EXIT_FAILURE);
		}
	}
}

void action_bin_decompress(Table * const table)
{
	int lastStrByteLenght, currCompressedByte, lastByte;
	long lastBytePos;
	VectorUC * currCode;
	currCode = VectorUC_malloc(table->count-1);
	lastBytePos = secure_ftell(table->compressedFile);
	lastByte = secure_fgetc(table->compressedFile);
	lastStrByteLenght = secure_fgetc(table->compressedFile);
	while ((currCompressedByte = secure_fgetc(table->compressedFile)) != EOF) {
		action_bin_decompress_byte(table, currCompressedByte, currCode, CHAR_BIT);
	}
	action_bin_decompress_byte(table, lastByte, currCode, lastStrByteLenght);
	currCode = Vector_free(currCode);
	secure_fseek(table->compressedFile, lastBytePos, SEEK_SET);
}

void action_fake_compress(Table * const table)
{
	int currPlainChar;
	size_t i;
	action_write_freq(table);
	secure_fputc(1,table->compressedFile);
	while ((currPlainChar = secure_fgetc(table->plainFile)) != EOF) {
		for (i = 0; i < table->code[currPlainChar]->cardinal; i++) {
			secure_fputc((int)table->code[currPlainChar]->value[i], table->compressedFile);
		}
	}
}

void action_fake_decompress(Table * const table)
{
	VectorUC * currCode;
	int currChrBit;
	currCode = VectorUC_malloc(table->count-1);
	while ((currChrBit = secure_fgetc(table->compressedFile)) != EOF) {
		currCode->value[currCode->cardinal] = currChrBit;
		currCode->cardinal++;
		if (action_write_matched_plainbyte(table, currCode)) {
			currCode->cardinal = 0;
		} else if (currCode->cardinal > currCode->capacity) {
			fprintf(stderr, "error : compressed file contains invalid code.\n");
			exit(EXIT_FAILURE);
		}
	}
	currCode = Vector_free(currCode);
}

void action_operate(Table * const table, char * plainFilePath, char * compressedFilePath, Action action)
{
	unsigned char compressionType;
	switch (action) {
		case COMPRESS:
			printf("compressing %s ...\n", plainFilePath);
			action_fake_compress(table);
			break;
		case BINCOMPRESS:
			printf("compressing %s ...\n", plainFilePath);
			action_bin_compress(table);
			break;
		default:
			printf("decompressing %s ...\n", compressedFilePath);
			compressionType = secure_fgetc(table->compressedFile);
			if (compressionType == 1) {
				action_fake_decompress(table);
			} else if (compressionType == 0) {
				action_bin_decompress(table);
			} else {
				fprintf(stderr, "error : wrong file structure.\n");
				exit(EXIT_FAILURE);
			}
			break;
	}
}

void action_display_compression_ratio(const size_t plainSize, const size_t compressedSize)
{
	double plainSizeKb, compressedSizeKb;
	float percentage;
	percentage = (1.0*compressedSize/plainSize)*100;
	plainSizeKb = (plainSize*8.0)/1024;
	compressedSizeKb = (compressedSize*8.0)/1024;
	printf("%0.1lf Kb compressed to %0.1lf Kb. (%0.1f%%)\n", plainSizeKb, compressedSizeKb, percentage);
}

void act(char * plainFilePath, char * compressedFilePath, Action action)
{
	Table * table;
	TableParam param;
	table = table_get();
	param.action = action;
	param.plainFilePath = plainFilePath;
	param.compressedFilePath = compressedFilePath;
	action_prepare_table(table, param);
	action_operate(table, plainFilePath, compressedFilePath, action);
	printf("done.\n");
	if (action != DECOMPRESS) {
		action_display_compression_ratio(secure_ftell(table->plainFile), secure_ftell(table->compressedFile));
	}
	action_recycle_table(table);
}
