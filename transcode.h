/**
 * \file transcode.h
 * \brief Contains transcode routines.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __TRANSCODE__
	#define __TRANSCODE__
	#include <limits.h>
	#include <stdlib.h>
	#include <math.h>

	/**
	 * \fn unsigned char transcode_str_to_byte(unsigned char strByte[UCHAR_MAX])
	 * \brief Transcode an 8 char string into 1 byte.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param strByte 8 characters string each character is either '1' or '0'.
	 * \return Byte version of the 8 characters string.
	 */
	unsigned char transcode_str_to_byte(unsigned char strByte[UCHAR_MAX]);

	/**
	 * \fn unsigned char * transcode_byte_to_str(unsigned char byte)param)
	 * \brief Transcode a byte into an 8 char string.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param byte Any byte.
	 * \return 8 characters string which corresponds to the value of a given byte.
	 */
	unsigned char * transcode_byte_to_str(unsigned char byte);
#endif
