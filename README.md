# A compression utility implementing Huffman code.

*if you have any request regarding the project please contact the author at <sangsoic@protonmail.com>*

## About

SHCCU stands for Simple Huffman Code Compression Utility. It allows to simply compress and decompress text files as well as visualizing Huffman code .

## Compile and execute.

1. Compiles and creates an executable file named *compress*.

> make

2. Executes the program. However, make sure you are in the same directory as where the *make* command was executed.

> ./compress -h

## Help menu.

Option | Description
-------|-------------
-c | compress using string format. (increase file size)
-b | compress using binary format. (decrease file size)
-d | decompress file. (automatically detects compression type)
-h | print help menu.

