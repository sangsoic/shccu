/**
 * \file action.h
 * \brief Routines interacting with files and table.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __ACTION__
	#define __ACTION__
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <limits.h>
	#include <time.h>
	#include <errno.h>
	#include "table.h"
	#include "secure.h"
	#include "transcode.h"
	#include "vector.h"

	/**
	 * \enum Action
	 * \brief Option of the program.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 */
	typedef enum {
		COMPRESS,
		DECOMPRESS,
		BINCOMPRESS
	} Action;

	/**
	 * \struct TableParam
	 * \brief Table parameters.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \var Action action
	 * Action of the program.
	 * \var char * plainFilePath
	 * plain file path.
	 * \var char * compressedFilePath
	 * compressed file path.
	 */
	typedef struct {
		Action action;
		char * plainFilePath;
		char * compressedFilePath;
	} TableParam;

	/**
	 * \fn void action_recycle_table(Table * const table)
	 * \brief Frees all table objects and prepares it for future use
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table targeted table address.
	 */
	void action_recycle_table(Table * const table);

	/**
	 * \fn void action_set_targets(Table * const table, const TableParam param)
	 * \brief Opens plain file and compressed file and store stream address inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table targeted table address.
	 * \param param table first parameters.
	 */
	void action_set_targets(Table * const table, const TableParam param);

	/**
	 * \fn void action_write_freq(Table * const table)
	 * \brief Writes character's frequency value inside compressed file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_write_freq(Table * const table);

	/**
	 * \fn void action_readfreq_from_plainfile(Table * const table)
	 * \brief Count characters occurrences of plain file and stores it inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_readfreq_from_plainfile(Table * const table);

	/**
	 * \fn void action_readfreq_from_compressedfile(Table * const table)
	 * \brief Read frequency table from compressed file and stores it inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_readfreq_from_compressedfile(Table * const table);

	/**
	 * \fn void action_prepare_table(Table * const table, const TableParam param)
	 * \brief Calls all necessary functions in order to prepare given table for either compression or decompression.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_prepare_table(Table * const table, const TableParam param);

	/**
	 * \fn void action_bin_compress(Table * const table)
	 * \brief Compress plain file (real binary compression).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_bin_compress(Table * const table);

	/**
	 * \fn bool action_write_matched_plainbyte(Table * const table, VectorUC * const code)
	 * \brief Decompress one character if huffman's code matches with one inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 * \param code Current code to decompress.
	 * \return true if it wrote a byte else false.
	 */
	bool action_write_matched_plainbyte(Table * const table, VectorUC * const code);

	/**
	 * \fn void action_bin_decompress_byte(Table * const table, unsigned char compressedByte, VectorUC * const codeBuffer, const unsigned char dataBitSize)
	 * \brief Decompress huffman code(s) currently in buffer.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 * \param compressedByte Compressed byte.
	 * \praram codeBuffer Current huffman code.
	 * \param dataBitSize Number of bits currently in buffer.
	 */
	void action_bin_decompress_byte(Table * const table, unsigned char compressedByte, VectorUC * const codeBuffer, const unsigned char dataBitSize);

	/**
	 * \fn void action_bin_decompress(Table * const table)
	 * \brief Decompress on byte.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_bin_decompress(Table * const table);

	/**
	 * \fn void action_fake_compress(Table * const table)
	 * \brief Compress plain file (convert file into a large string of huffman's code).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_fake_compress(Table * const table);

	/**
	 * \fn void action_fake_decompress(Table * const table)
	 * \brief Decompress plain file (convert large string of huffman's code into ASCII file).
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void action_fake_decompress(Table * const table);

	/**
	 * \fn void action_operate(Table * const table, char * plainFilePath, char * compressedFilePath, Action action)
	 * \brief Executes given action.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 * \param plainFilePath Plain file path.
	 * \param compressedFilePath Compressed file path.
	 * \param action Action to perform.
	 */
	void action_operate(Table * const table, char * plainFilePath, char * compressedFilePath, Action action);

	/**
	 * \fn void action_display_compression_ratio(const size_t plainSize, const size_t compressedSize)
	 * \brief Prints out compression ratio once compression is done.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param plainSize plain file size.
	 * \param compressedSize compressed file size.
	 */
	void action_display_compression_ratio(const size_t plainSize, const size_t compressedSize);

	/**
	 * \fn void act(char * plainFilePath, char * compressedFilePath, Action action)
	 * \brief Executes all action and table routines.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param plainFilePath Plain file path.
	 * \param action Action to perform.
	 */
	void act(char * plainFilePath, char * compressedFilePath, Action action);
#endif
