/**
 * \file secure.h
 * \brief Test for potential error when executing standard stream routines.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __SECURE__
	#define __SECURE__
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <errno.h>
	

	/**
	 * * \fn FILE * secure_fopen(const char * pathName, const char * mode)
	 * * \brief Same as fopen standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	FILE * secure_fopen(const char * pathName, const char * mode);

	/**
	 * * \fn FILE * secure_fclose(FILE * stream);
	 * * \brief Same as fclose standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	FILE * secure_fclose(FILE * stream);

	/**
	 * * \fn void secure_fputc(const int byte, FILE * const stream)
	 * * \brief Same as fputc standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	void secure_fputc(const int byte, FILE * const stream);
	
	/**
	 * * \fn int secure_fgetc(FILE * const stream)
	 * * \brief Same as fgetc standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	int secure_fgetc(FILE * const stream);

	/**
	 * * \fn size_t secure_fwrite(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream)
	 * * \brief Same as fwrite standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	size_t secure_fwrite(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream);

	/**
	 * * \fn size_t secure_fread(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream)
	 * * \brief Same as fread standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	size_t secure_fread(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream);

	/**
	 * * \fn long secure_ftell(FILE * const stream)
	 * * \brief Same as ftell standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	long secure_ftell(FILE * const stream);

	/**
	 * * \fn void secure_fseek(FILE * const stream, const long offset, const int whence)
	 * * \brief Same as fseek standard function but test for potential error.
	 * * \author sangsoic <sangsoic@protonmail.com>
	 * * \version 0.1
	 * * \date 23 January 2020
	 * */
	void secure_fseek(FILE * const stream, const long offset, const int whence);
#endif
