/**
 * \file option.h
 * \brief Contains routines which interface with the user.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __OPTION__
	#define __OPTION__
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "action.h"

	/**
	 * \fn void option_help(void)
	 * \brief Prints out help menu on standard output.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 */
	void option_help(void);

	/**
	 * \fn void option_help(void)
	 * \brief Executes program's routine depending on arguments passed throw command line.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \return Status code of the program.
	 */
	int option_choice(int argc, char ** argv);

	/**
	 * \fn void option_feedback(int status)
	 * \brief Receives current exit status code and prints out corresponding error message on standard error output.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param status Current exit status code.
	 */
	void option_feedback(int status);

	/**
	 * \fn int run(int argc, char ** argv)
	 * \brief First routine called, responsible for calling all others.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param agrc Number of shell words passed throw command line.
	 * \param argv String representing the arguments.
	 * \return Exit status code.
	 */
	int run(int argc, char ** argv);
#endif
