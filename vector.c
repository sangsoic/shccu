/**
 * \file vector.c
 * \brief Contains routines handling dynamic array generically.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "vector.h"

Vector * Vector_malloc(const size_t capacity, const size_t atomicSize)
{
	Vector * vector;
	if (atomicSize == 0 || capacity == 0) {
		fprintf(stderr,"error : cannot  allocate 0 sized objects.\n");
		exit(EXIT_FAILURE);
	}
	vector = malloc(sizeof(Vector));
	if (vector == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	vector->value = malloc(atomicSize * capacity);
	if (vector->value == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	vector->capacity = capacity;
	vector->cardinal = 0;
	return vector;
}

void * Vector_free(void * vectorAddress)
{
	Vector * vector;
	vector = vectorAddress;
	free(vector->value);
	vector->value = NULL;
	vector->cardinal = 0;
	vector->capacity = 0;
	free(vector);
	return NULL;
}

void Vector_rshift(void * const address, const size_t shiftSize, const size_t offset)
{
	size_t i, j;
	Byte * previous, * cursor, curr;
	cursor = address;
	previous = malloc(offset);
	if (previous == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (j = 0; j < offset; j++) {
		previous[j] = cursor[j];
	}
	for (i = offset; i < offset*shiftSize; i++, j++) {
		curr = cursor[i];
		cursor[i] = previous[j%offset];
		previous[j%offset] = curr;
	}
	free(previous);
}

void Vector_lshift(void * const address, const size_t shiftSize, const size_t offset)
{
	size_t i, j, cursorIndex;
	Byte * previous, * cursor, curr;
	cursor = address;
	previous = malloc(offset);
	if (previous == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	for (j = 0; j < offset; j++) {
		previous[j] = cursor[(shiftSize-1)*offset+j];
	}
	for (i = offset; i < shiftSize*offset; i+=offset) {
		for (j = 0; j < offset; j++) {
			cursorIndex = ((shiftSize-1)*offset)-i+j;
			curr = cursor[cursorIndex];
			cursor[cursorIndex] = previous[j];
			previous[j] = curr;
		}
	}
	free(previous);
}

bool Vector_cmp(const Vector * const vector0, const size_t atomicSize0, const Vector * const vector1, const size_t atomicSize1)
{
	size_t i, byteCardinal0, byteCardinal1;
	Byte * cursor0, * cursor1;
	i = 0;
	cursor0 = vector0->value;
	cursor1 = vector1->value;
	byteCardinal0 = vector0->cardinal * atomicSize0;
	byteCardinal1 = vector1->cardinal * atomicSize1;
	if (byteCardinal0 == byteCardinal1) {
		while ((i < byteCardinal0) && (cursor0[i] == cursor1[i])) {
			i++;
		}
	}
	return 2*i == byteCardinal0+byteCardinal1;
}
