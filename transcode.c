/**
 * \file transcode.c
 * \brief Contains transcode routines.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "transcode.h"

unsigned char transcode_str_to_byte(unsigned char strByte[CHAR_BIT])
{
	unsigned char byte;
	size_t i;
	byte = 0;
	for (i = 0; i < CHAR_BIT; i++) {
		byte += (strByte[i]-'0') * pow(2,i);
	}
	return byte;
}

unsigned char * transcode_byte_to_str(unsigned char byte)
{
	static unsigned char strByte[CHAR_BIT];
	size_t i;
	for (i = 0; i < CHAR_BIT; i++) {
		strByte[i] = ((byte % (unsigned long)pow(2,i+1)) / (unsigned long)pow(2,i)) + '0';
	}
	return strByte;
}


